const functions = require('firebase-functions');
const admin = require('firebase-admin');
const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');

var sizeOf = require('image-size');
var ColorThief = require('color-thief-jimp');
var Jimp = require('jimp');

const MAX_SIZE = 2000;


admin.initializeApp();

exports.filtroImagen = functions.storage.object().onFinalize((object) => {
  const fileBucket = object.bucket;
  const filePath = object.name;
  const contentType = object.contentType;
  const metadata = {
    contentType: contentType
  };
  const fileName = path.basename(filePath);
  const tempFilePath = path.join(os.tmpdir(), fileName);

  //Comprueba que es una imagen
  if (!contentType.startsWith('image/')) {
    console.log(filePath, 'No es imagen');
    return null;
  }
  //Comprueba que viene de inbox
  if (!filePath.startsWith('inbox/')) {
    return null;
  }

  //Inicializamos el Google Cloud Storage para descargar el archivo
  const gcs = admin.storage();
  const bucket = gcs.bucket(fileBucket);

  return bucket.file(filePath).download({
    destination: tempFilePath
  }).then(
    () => {
        console.log("Se descargo el archivo a la carpeta temporal", tempFilePath)
        var dimensions = sizeOf(tempFilePath);
        if (dimensions.width > MAX_SIZE || dimensions.height > MAX_SIZE ) {
          console.log("Imagen grande. Ha sido eliminada");
          return null;
        } else {
          console.log("Imagen aceptada");

          const pathDefinitivo = path.join('images', fileName);
          return bucket.upload(tempFilePath, {
            destination: pathDefinitivo,
            metadata: metadata
          });
        }
    }
  ).then(
    () => bucket.file(filePath).delete()
  ).then(
    () => fs.unlinkSync(tempFilePath)
  );
});

exports.thumbnail = functions.storage.object().onFinalize((object) => {
  const fileBucket = object.bucket;
  const filePath = object.name;
  const contentType = object.contentType;
  const metadata = {
    contentType: contentType
  };
  const fileName = path.basename(filePath);
  const tempFilePath = path.join(os.tmpdir(), fileName);

  //Comprueba que es una imagen
  if (!contentType.startsWith('image/')) {
    console.log(filePath, 'No es imagen');
    return null;
  }
  //Comprueba que viene de images
  if (!filePath.startsWith('images/')) {
    return null;
  }

  //Inicializamos el Google Cloud Storage para descargar el archivo
  const gcs = admin.storage();
  const bucket = gcs.bucket(fileBucket);

  return bucket.file(filePath).download({
    destination: tempFilePath
  }).then(
    () => {
      console.log('Se descargo el archivo a la carpeta temporal en ', tempFilePath);
      // Generamos el thumbnail usando ImageMagick.
      return spawn('convert', [tempFilePath, '-thumbnail', '200x200>', tempFilePath]);
    }
  ).then(
    () => {
      console.log('Thumbnail creado en ', tempFilePath);
      // Agregamos el prefijo 'thumb_' al nombre.
      const thumbFileName = `thumb_${fileName}`;
      const thumbFilePath = path.join('thumbs', thumbFileName);
      // Subimos el archivo a thumbs.
      return bucket.upload(tempFilePath, {
        destination: thumbFilePath,
        metadata: metadata
      });
    }
  ).then(
    () => fs.unlinkSync(tempFilePath)
  );
});


exports.clasificar = functions.storage.object().onFinalize((object) => {
  const fileBucket = object.bucket;
  const filePath = object.name;
  const contentType = object.contentType;
  const metadata = {
    contentType: contentType
  };
  const fileName = path.basename(filePath);
  const tempFilePath = path.join(os.tmpdir(), fileName);

  //Comprueba que es una imagen
  if (!contentType.startsWith('image/')) {
    console.log(filePath, 'No es imagen');
    return null;
  }
  //Comprueba que viene de image
  if (!filePath.startsWith('images/')) {
    return null;
  }

  //Inicializamos el Google Cloud Storage para descargar el archivo
  const gcs = admin.storage();
  const bucket = gcs.bucket(fileBucket);

  return bucket.file(filePath).download({
    destination: tempFilePath
  }).then(
    () => {
      console.log('Se descargo el archivo a la carpeta temporal en ', tempFilePath);
      // Generamos el thumbnail usando ImageMagick.
      return Jimp.read(tempFilePath);
    }
  ).then(
    objImg => {
      var color = ColorThief.getColor(objImg);
      var hsl = rgbToHsl(color[0], color[1], color[2]);
      var l = hsl[2];
      var tipo = '';
      if (l < 0.5) {
        console.log("Imagen oscura.");
        tipo = 'OSCURA';
      }else {
        console.log("Imagen clara.");
        tipo = 'CLARA';
      }

      return admin.database().ref(`/imagenes/${tipo}/`).push({filePath: filePath});
    }
  ).then(
    snapshot => {
      console.log("Imagen clasificada correctamente.");
      return fs.unlinkSync(tempFilePath);
    }
  );
});

/*
exports.noficarEmail = functions.database.ref('/imagenes/{tipo}/{id}/filePath').onCreate({
  (snapshot, context) => {
    const filePath = snapshot.val();
    const tipo = context.params.tipo;
    if (!snapshot.changed('subscribedToMailingList')) {
      return null;
    }

    const mailOptions = {
      from: '"Spammy Corp." <noreply@firebase.com>',
      to: val.email,
    };

    const subscribed = val.subscribedToMailingList;

    // Building Email message.
    mailOptions.subject = subscribed ? 'Thanks and Welcome!' : 'Sad to see you go :`(';
    mailOptions.text = subscribed ?
        'Thanks you for subscribing to our newsletter. You will receive our next weekly newsletter.' :
        'I hereby confirm that I will stop sending you the newsletter.';

    try {
      await mailTransport.sendMail(mailOptions);
      console.log(`New ${subscribed ? '' : 'un'}subscription confirmation email sent to:`, val.email);
    } catch(error) {
      console.error('There was an error while sending the email:', error);
    }
  return null;
  }
});
*/

/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   {number}  r       The red color value
 * @param   {number}  g       The green color value
 * @param   {number}  b       The blue color value
 * @return  {Array}           The HSL representation
 */

function rgbToHsl(r, g, b){
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max === min){
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [h, s, l];
}
